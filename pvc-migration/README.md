# PVC Migration
Ce script est utilisé pour change un pvc **MYSQL** de storage class.

## Usage
Commencez par editer le script pour modifier la variable `NEW_STORAGECLASS`.
Ensuite

```bash
./migrate.sh <PROJECT> <RESOURCE>
```
Il est nécéssaire d'être connecté au cluster auparavant avec oc
