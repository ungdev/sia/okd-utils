#!/bin/bash
# Don't forget to login before

# Will delete all evicted pods in the cluster

pods=$(oc get pods --all-namespaces | grep Terminating | awk '{print $1, $2}')

while read pod; do
  namespace=$(echo $pod | awk '{print $1}')
  pod=$(echo $pod | awk '{print $2}')
  #echo NS: $namespace
  #echo POD: $pod
  oc delete --namespace=$namespace pods/$pod --grace-period=0 --force
done <<< "$pods"
